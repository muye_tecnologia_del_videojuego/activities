public class capsuleMovement : MonoBehaviour{
    [SerializeField]
    private vector3 direction;
    public float speed;

    void Update(){
        direction = clampvector3(direction);

        transform.Translate(direction * (speed * time.dletatime));   
}

public static vector3 clampvector3(vector3 target) {
        float clampedX = Mathf.clamp(target.x, -1f, 1f);
        float clampedY = Mathf.clamp(target.y, -1f, 1f);
        float clampedZ = Mathf.clamp(target.z, -1f. 1f);

        clampvector3 result = new vector3(clampedX, clampedY, clampedZ);

        return result;
    }